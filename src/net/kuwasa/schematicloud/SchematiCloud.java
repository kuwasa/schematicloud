package net.kuwasa.schematicloud;

import java.io.File;

import net.kuwasa.schematicloud.SchematiCloudCommand;

import org.bukkit.plugin.java.JavaPlugin;

public class SchematiCloud extends JavaPlugin {
	public String config_stmpdir;
	public File config_ftmpdir;
	
    public void onEnable() {
		getCommand("schc").setExecutor(new SchematiCloudCommand(this));
    	loadconfig();
    }
    public void loadconfig(){
		saveDefaultConfig();
		getConfig().options().copyDefaults(true);
		this.reloadConfig();
		config_stmpdir = this.getConfig().getString("tmpdir");
		if (config_stmpdir.contains("NULL")){
			//初期設定の場合
			config_stmpdir = this.getDataFolder() + "/downloads/";
		}
		config_ftmpdir = new File(config_stmpdir);
    }
}
