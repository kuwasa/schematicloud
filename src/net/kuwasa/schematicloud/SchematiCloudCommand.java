package net.kuwasa.schematicloud;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EmptyClipboardException;
import com.sk89q.worldedit.FilenameException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitPlayer;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import net.kuwasa.schematicloud.SchematiCloud;

public class SchematiCloudCommand implements CommandExecutor {
	SchematiCloud plugin;
	public SchematiCloudCommand(SchematiCloud plugin) {
		this.plugin = plugin;
	}
	private String result;
	private String b64file;
	private Player player;
	private String key;
	private static final String PATH = "tmp.schematic";


	private CuboidClipboard cc;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		if (cmd.getName().equalsIgnoreCase("schc")) {
			// 変数宣言
			Player s = (Player) sender;
			// Boolean onlinemode = sender.getServer().getOnlineMode();
			WorldEditPlugin we = (WorldEditPlugin) this.plugin.getServer().getPluginManager().getPlugin("WorldEdit");
			final LocalSession session = we.getSession(s);
			final WorldEdit we2 = we.getWorldEdit();
			final BukkitPlayer localPlayer = we.wrapPlayer(s);
			SchematicFormat format = SchematicFormat.getFormat("mcedit");
			
			if (cmd.getName().equalsIgnoreCase("schc")) {

				/*
				 * Get File Command.
				 */
			
				if (Arrays.asList(args).contains("get")) {
					if (!chkcommand(2,args,sender)){
						return true;
					}
					if (session == null) {
						s.sendMessage(ChatColor.RED+ "[SchematiCloud] セッションを取得できませんでした。");
						return true;
					}
					s.sendMessage(ChatColor.GREEN + "[SchematiCloud] ダウンロードします。");
					player = (Player) sender;
					key = args[1];
					Bukkit.getServer().getScheduler().runTaskAsynchronously(this.plugin, new Runnable() {
						@Override
						public void run() {
							try {
								URL TestURL = new URL("http://kuwasa.net/schematicloud/api/v1/get.php");
								URLConnection con = TestURL.openConnection();
								con.setDoOutput(true);
								OutputStreamWriter ow1 = new OutputStreamWriter(con.getOutputStream());
								BufferedWriter bw1 = new BufferedWriter(ow1);
								bw1.write("key=" + key);
								bw1.close();
								ow1.close();
								InputStreamReader ir1 = new InputStreamReader(con.getInputStream());
								BufferedReader br1 = new BufferedReader(ir1);
								String line;
								while ((line = br1.readLine()) != null) {
									result = line;
								}
								br1.close();
								ir1.close();
								if (result.matches("fail")) {
									player.sendMessage(ChatColor.RED + "[SchematiCloud] プログラムのエラーによりアップロードに失敗しました。");
								} else if (result.matches("404")) {
									player.sendMessage(ChatColor.RED + "[SchematiCloud] ファイルが見つかりませんでした。");
								} else {
									File dirs = plugin.config_ftmpdir;
									if (!dirs.exists()) {
										dirs.mkdirs(); // make folders
									}
									byte[] resultd = Base64.decode(result);// デコード処理
									FileOutputStream fileOutStm = null;
									try {
										fileOutStm = new FileOutputStream(plugin.config_stmpdir+ key+ ".schematic");
									} catch (FileNotFoundException e1) {
										System.out.println("ファイルが見つからなかった。");
									}
									try {
										fileOutStm.write(resultd);
									} catch (IOException e) {
										System.out.println("入出力エラー。");
									}
									File f = we2.getSafeOpenFile(localPlayer, dirs, key,"schematic", "schematic");
									if (!f.exists()) {
										player.sendMessage(ChatColor.RED + "[SchematiCloud] ダウンロードしたファイルが見つかりませんでした。");
										return;
									}
									String formatName = null;
									SchematicFormat format = formatName == null ? null : SchematicFormat.getFormat(formatName);
									if (format == null) {
										format = SchematicFormat.getFormat(f);
									}
									if (format == null) {
										player.sendMessage(ChatColor.RED+ "[SchematiCloud] 未知のフォーマットです。 フォーマット:"+ format);
										return;
									}
									try {
										String filePath = f.getCanonicalPath();
										String dirPath = dirs.getCanonicalPath();
										if (!filePath.substring(0,dirPath.length()).equals(dirPath)) {
											player.sendMessage("Schematic could not read or it does not exist.");
										} else {
											session.setClipboard(format.load(f));
											WorldEdit.logger.info(player.getName() + " loaded " + filePath);
											player.sendMessage(ChatColor.GREEN + "[SchematiCloud] ダウンロードしました。 //pasteで貼付けられます。");
										}
									} catch (DataException e) {
										player.sendMessage("Load error: " + e.getMessage());
									} catch (IOException e) {
										player.sendMessage("Schematic could not read or it does not exist: " + e.getMessage());
									}
								}
							} catch (Exception e) {
								player.sendMessage("[SchematiCloud] HTTP通信に失敗しました。\n" + e.getCause());

							}
						}
					});
					return true;
				}
				
				/*
				 * Save File Command
				 */
				if (Arrays.asList(args).contains("save")) {
					File pluginDir = this.plugin.getDataFolder();
					if (session == null) {
						s.sendMessage(ChatColor.RED + "[SchematiCloud] セッションを取得できませんでした。");
						return true;
					}
					try {
						cc = session.getClipboard();
					} catch (EmptyClipboardException e) {
						s.sendMessage(ChatColor.RED + "[SchematiCloud] 最初に //copy コマンドを実行する必要があります。");
						return true;
					}

					File f;
					try {
						f = we2.getSafeSaveFile(localPlayer, pluginDir,sender.getName() + "_" + "tmp", "schematic",new String[] { "schematic" });
						format.save(cc, f);
						s.sendMessage(ChatColor.GREEN + "[SchematiCloud] アップロードします。");
					} catch (FilenameException ex) {
						sender.sendMessage("ERROR:" + ex.getMessage());
					} catch (IOException ex) {
						sender.sendMessage("ERROR:" + ex.getMessage());
					} catch (DataException ex) {
						sender.sendMessage("ERROR:" + ex.getMessage());
					}

					/*
					 * ファイルをバイナリとして読み込む
					 */
					File file = new File(pluginDir + "/" + sender.getName() + "_" + PATH);
					FileInputStream input;
					//sender.sendMessage("" + file);
					try {
						input = new FileInputStream(file);
						byte[] data = new byte[(int) file.length()];
						input.read(data);
						input.close();
						b64file = Base64.encode(data);
					} catch (FileNotFoundException ex) {
						sender.sendMessage(ChatColor.RED + "[SchematiCloud] ファイルが読み込めなかったため、アップロードができませんでした。");
						return true;
					} catch (IOException ex) {
						sender.sendMessage(ChatColor.RED + "[SchematiCloud] ファイル変換ができませんでした。" + ex.getMessage());
						return true;
					}
					player = (Player) sender;
					Bukkit.getServer().getScheduler().runTaskAsynchronously(this.plugin, new Runnable() {
								@Override
								public void run() {
									try {
										URL TestURL = new URL("http://kuwasa.net/schematicloud/api/v1/upload.php");
										URLConnection con = TestURL.openConnection();
										con.setDoOutput(true);
										OutputStreamWriter ow1 = new OutputStreamWriter(con.getOutputStream());
										BufferedWriter bw1 = new BufferedWriter(ow1);
										bw1.write("user=" + player.getName() + "&file=" + b64file + "￥");
										bw1.close();
										ow1.close();

										InputStreamReader ir1 = new InputStreamReader(con.getInputStream());
										BufferedReader br1 = new BufferedReader(ir1);
										String line;
										while ((line = br1.readLine()) != null) {
											result = line;
										}
										if (Arrays.asList(result).contains("CANCEL")) {
											player.sendMessage(ChatColor.RED + "[SchematiCloud] このサーバからのアップロードは許可されていません。\nログインしてアップロードを許可しましょう。");
											player.sendMessage(ChatColor.RED + "http://kuwasa.net/schematicloud/user.php?p=whitelist");
										} else {
											player.sendMessage(ChatColor.GREEN + "[SchematiCloud] アップロードしました。");
											player.sendMessage(ChatColor.GREEN + result);
										}
										br1.close();
										ir1.close();
									} catch (Exception e) {
										player.sendMessage(ChatColor.RED + "[SchematiCloud] HTTP通信に失敗しました。\n"+ e.getMessage());
									}
								}
							});
					File fileSchematic = new File("" + pluginDir + "/" + sender.getName() + "_" + "tmp.schematic");
					fileSchematic.delete();
					return true;
				}	
				
			}
			chkcommand(10, args, s);//10個も引数付ける人がいないと信じてる
			return true;
		}
		return true;
	}
	
	public boolean chkcommand(Integer i, String[] s, CommandSender s2) {
		if (s.length != i) {
			s2.sendMessage(ChatColor.AQUA + "[SchematiCloud] コマンドの使い方が正しくありません。\n"
					+ "/schc save\n"
					+ "/schc get <FILEID>");
			return false;
		}
		return true;
	}
}
